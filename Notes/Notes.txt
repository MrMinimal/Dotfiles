Notes so I don't have to revisit purple Stack Overflow links in shame.


=============================== Misc ==========================================
# Run command detached (keeps running outside ssh session)
nohup /path/to/program &

# Generate md5 hashes
echo -n <stringToHash> | md5sum -t

# Combine PDFs (don't forget the output name)
pdfunite part1.pdf part2.pdf out.pdf


============================== Hardware =======================================
# List all connected devices, drives, usbs
lsblk

# Show drive fill state
df

# Write image to USB
dd bs=4M if=2017-09-07-raspbian-stretch.img of=/dev/sdX conv=fsync status=progress

# Mount .img file
fdisk /path/file.img -l     # Blocksize * startOffset of partition to mount
mount -o loop,offset=<calcSolution> /path/file.img /mnt/tmp

# Disable raspberry pi zero power led
vim /boot/config.txt
# put:
dtparam=act_led_trigger=none
dtparam=act_led_activelow=on


================================ Files ========================================
# Unpack zip in new folder
unzip filename.zip -d customFolderName

# Unpack tar in new folder
tar -xzvf filename.tar.gz -C customFolderName

# Create links
ln

# Create (compressed) backup via ssh
ssh root@raspberrypi dd if=/dev/mmcblk0 | gzip -c > img.gz


================================ Search =======================================
# Find file by name (case sensitive)
find -name "query"

# Find file by name (case insensitive)
find -iname "query"

# Find files/directories (f for file, d for directory, l for link)
find -type type_descriptor query

# Find files with ending
find / -type f -name "*.conf"

# Find files bigger than
find / -size +700M

# Find files which have been modified after yesterday
find / -mtime +1

# Find files which have been modified before yesterday
find / -mtime -1

# Find files with at least these permissions
find / -perm -644


=============================== Network =======================================
# Show all open ports
netstat -vatn

# Show all ips in network
nmap -sn 192.168.0.0/24

# Show all ips in network (without nmap)
ping <broadcastIP>  # maybe with -b
arp -a

# Get IP of device to device connection (direct ethernet)
# Connect two devices directly via LAN
# Needs dnsmasq-base to be installed and
# ipv4 set to "shared to other computers"
cat /var/lib/misc/dnsmasq.leases

# Setup WLAN
sudo iwlist wlan0 scan                              # List all networks
vim /etc/wpa_supplicant/wpa_supplicant.conf
# Add this output to wpa_passphrase.conf:
wpa_passphrase "testing" "testingPassword"
wpa_cli -i wlan0 reconfigure                        # restart the wlan

# Disable password identification
# vim /etc/ssh/sshd_config 
# 
# ChallengeResponseAuthentication no
# PasswordAuthentication no
# UsePAM no

# Change hostname
vim /etc/hostname

# Copy ssh public keys over
ssh-copy-id user@ip


=============================== Docker ========================================
# Show all images
sudo docker images

# Remove an image
sudo docker rmi <imageHash>

# Remove a container
sudo docker rm <containerHash>

# Stop a container gracefully
sudo docker stop <containerHash>

# Attach bash to container
sudo docker exec -it <containerHash> bin/bash


================================ Git ==========================================
# Undo all local commits, keep files edited
git reset HEAD~

# Undo all local commits, DELETE changes in files
git reset HEAD~

# Show diff of staged files as well
$ git diff --staged

# Show fancy diff when commiting
git commit --verbose

# Show if repo uses https or ssh
git remote show origin

# Fix remote URL
git remote set-url origin git+ssh://git@github.com/username/reponame.git


================================ Vim ==========================================
# Edit a remote file with current instance of vim
e! scp://pi@192.168.0.21//home/pi/Dev/pzzioServer.py

# replace with prompt in current file
:%s/old/new/gc

# Open all files with .cpp extension
:arg *.cpp

# Replace in all open buffers
:bufdo %s/pattern/replace/ge | update

# Create html page of current file (with syntax highlight)
:TOhtml

# Indentation pasting
]p

# Replace tabs
retab!

# Replace all trailing whitespaces
:%s/\s\+$//e

# Format selected
=

# Convert selected to uppercase/lowercase
gu gU

# Last insert location
gi

# Last visual selection
gv

# Fix spelling
z=

# Next spell mistake
[z


=============================== Gnome =========================================
# Open the terminal in fullscreen mode
gsettings set org.gnome.desktop.default-applications.terminal exec 'terminal'


============================ Programming ======================================
# Compile godot for raspberry pi 2b
nohup scons -j2 platform=server tools=no module_webm_enabled=no target=release bits=64 &
