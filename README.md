# Purpose
This holds all of my Linux configuration files which are all placed in the
$HOME directory.

# Dependencies
## .bashrc
* airline - better UI

## .vimrc
* vundle - plugin manager
* vim airline - better UI
* vim-airline-themes
* flattened - solarized colorscheme

# Getting started
Clone this repository to your machine
```bash
git clone https://gitlab.com/MrMinimal/Dotfiles.git --depth=1
```

Change into the repo you just cloned
```bash
cd Dotfiles/
```

Link the dotfiles you want to use to your $HOME directory
```bash
# Config for the vim editor
mv .vimrc $HOME

# Config for bash
mv .bashrc $HOME
```
